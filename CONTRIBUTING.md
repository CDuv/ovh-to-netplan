# To contribute to the project

## Git commits

Commits in this project follow the [*Conventional Commits* specification v1.0.0](https://www.conventionalcommits.org).

Allowed scopes are:

* *deps*: Changes in dependencies (eg. version upgrade).
* *os*: Changes in the code that reads configuration in the host's OS.
* *reader*: Changes in the code that reads *OVHcloud* API.
* *writer*: Changes in the code that writes *Netplan* configurations.
* *ovh*: Changes related to or to cope with any external *OVHcloud* changes (eg. API depreciation).
* *netplan*: Changes related to or to cope with any external *Netplan* changes.

## Code source

Source code files must:

* Use *Linux type* end of line (*EOL*): `LF`
* Ends by a *newline* character (id. had an empty line at the end).

### Standards

Respect of coding standards is checks by [PHP Coding Standards Fixer](http://cs.sensiolabs.org).
The `.php_cs.dist` file contains the list of settings to apply.

The following command lists infringements (and their corrections):

```Shell
./vendor/bin/php-cs-fixer fix -v --dry-run --diff --using-cache=no
```

(Remove the `--diff` parameter to only list files without the changes)

The following command fixes infringements:

```Shell
./vendor/bin/php-cs-fixer fix
```

### Code format

To make sure the PHP code is correctly formatted (eg. indentation)
[PHP Code Beautifier and Fixer](https://github.com/squizlabs/PHP_CodeSniffer)
is used.
The `.phpcbf-ruleset.xml` file contains the list of settings to apply.

```Shell
./vendor/bin/phpcbf \
    -d date.timezone=UTC \
    --basepath="$(pwd)/" \
    --standard=.phpcbf-ruleset.xml
```

### PHP compatibility

To make sure the PHP code is compatible with minimal required PHP version,
[PHP Compatibility Coding Standard for PHP_CodeSniffer](https://github.com/PHPCompatibility/PHPCompatibility) is used:

```Shell
./vendor/bin/phpcs \
    -d date.timezone=UTC \
    --basepath="$(pwd)/" \
    --standard=.phpcs-ruleset.xml
```
