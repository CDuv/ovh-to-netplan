# Script to convert *OVHcloud* IP routing information to *Netplan* configuration

This scripts reads the IP routing information from the [*OVHcloud*'s API](https://api.ovh.com)
and builds [*Netplan*](https://netplan.io) configuration files.

This allows a server to automatically get and use the IP associated to it on the
*OVHcloud* side (API or GUI *OVH Manager*).

## Why this script?

When I bought/added/removed an IP from *OVHcloud* for a dedicated server, I had
to reconfigure it's OS so that it could use the IP: it was a source of mistakes
which, being network-related, could be fatal.
This script makes OS configuration automatic and prevent such errors.

## How does it work?

1. The script detects the public IP of the server it is running on.
1. Gets the *OVHcloud* dedicated server from this IP (uses *OVHcloud* API).
1. Gets the all IP addresses routed to the found dedicated server (uses *OVHcloud* API).
1. Detects some local network configuration (gateway and DNS).
1. Builds complete *Netplan* configuration, spitted in 2 files :
   * One for the DNS, gateway and dedicated IP addresses.
   * One for the failover IP addresses.

Notes:

* The script does not installs [*Netplan*](https://netplan.io).
* The script does not automatically detects IP routing changes on *OVHcloud*: it
  fetches informations **when** executed.
* The script writes *Netplan* configuration to `/etc/netplan` (path is configurable).
* The script does not trigger a `netplan apply`, it is up to you to run it.
* Generated *Netplan* configurations uses:
  * Version 2.
  * The `networkd` renderer.

## Configuration

Configuration for the script is stored in the `config/` directory.

The main configuration file is `config.php`.

If present, the script will read a secondary configuration file `config.override.php`
whose content will override settings of `config.php`.

See `config.override.example.php` for an example of a `config.override.php` file.

### *OVHcloud* API

The script uses the *OVHcloud* API, so it needs the following API credentials (to set
in the `ovh_api.auth` configuration key):

* application_key
* application_secret
* consumer_key

The script uses only uses the following API routes, and only to read data
(`GET` HTTP method):

* `/ip`
* `/ip/{ip}`
* `/dedicated/server`
* `/dedicated/server/{serviceName}`
* `/dedicated/server/{serviceName}/ips`

Here is an URL to generate a token for the script that includes the name, the
description and the rights:

`https://api.ovh.com/createToken/index.cgi?applicationName=ovh_to_netplan&applicationDescription=Fetches%20IPs%20from%20OVH%20API%20to%20fill%20Netplan.io%20config&duration=-1&GET=/ip&GET=/ip/*&GET=/dedicated/server&GET=/dedicated/server/*&GET=/dedicated/server/*/ips`

## Requirements

* `ip`, `grep` and `awk` commands
* *PHP* (see `composer.json` for PHP requirements)
* [*Composer*](https://getcomposer.org) (only for installation of dependencies)

## Installation

1. Get the source code (via `git clone`, source tarball or release tarball) and
   place it in some directory.
1. Configure the application by populating the `config.php` and/or
   `config.override.php` file (see *Configuration* part).
1. Run `composer install --no-dev` to fetch PHP dependencies if required.

### Example of a complete installation on a *Debian* system

```Shell
install_dirpath="/opt/ovh_to_netplan"
config_dirpath="/etc/ovh_to_netplan"
log_dirpath="/var/log/ovh_to_netplan"

apt-get install -y \
    php7.3-json \
    php7.3-mbstring \
    php7.3-yaml

mkdir --parents "${install_dirpath}" "${config_dirpath}" "${log_dirpath}"

# Installation using git clone
git clone https://gitlab.com/CDuv/ovh-to-netplan.git "${install_dirpath}"
composer install --no-dev

# Store configuration in /etc:
mv "${install_dirpath}/config/"* "${config_dirpath}"
rmdir "${install_dirpath}/config"
ln --symbolic "${config_dirpath}" "${install_dirpath}/config"

# (Now is a good time to fill the configuration settings in config.override.php)

# Scheduled task (every 10 minutes):
cat > "/etc/cron.d/ovh_to_netplan" <<EOT
    */10    *    *   *   *   root    php "${install_dirpath}/bin/write_netplan.php" >> "${log_dirpath}/write_netplan.log" 2>&1 && /usr/sbin/netplan apply
EOT

# Logs rotation:
cat > "/etc/logrotate.d/ovh_to_netplan" <<EOT
${log_dirpath}/*.log {
    daily
    missingok
    rotate 32
    compress
    delaycompress
    notifempty
    dateext
    dateyesterday
    dateformat -%Y-%m-%d-%s
}
EOT
```

## Usage / Execution

```Shell
php bin/write_netplan.php
```

## Example of generated Netplan configuration

*Base* configuration file:

```YAML
---
network:
  version: 2
  renderer: networkd
  ethernets:
    eno1:
      addresses:
      - 2001:db8:1234:5678:9::abcd:ef/64
      - 213.186.33.6/24
      dhcp4: false
      dhcp6: false
      gateway4: 213.186.33.254
      gateway6: 2001:db8:1234:56ff:ff:ff:ff:ff
      nameservers:
        search:
        - ovh.net
        addresses:
        - 213.186.33.99
        - 2001:41d0:3:163::1
      routes:
      - to: 2001:db8:1234:56ff:ff:ff:ff:ff
        scope: link
...
```

*Extra*/*Failover* configuration file:

```YAML
---
network:
  ethernets:
    eno1:
      addresses:
      - 146.59.212.117/32
      - 146.59.188.146/32
...
```

## Special thanks

This script was primarly made at and for the use of [*CyberCité*](https://www.cybercite.fr)
(my employer), but open-sourced once ready to go to production. Thanks to them
for agreeing to give back to the open-source community.

## Contributing

This project is open-source and any help/fork/contributions is welcomed, please
refer to the [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details on how to
contribute.

## Licensing

See the [`LICENSE`](LICENSE) file for licensing information about this script.

## Roadmap

* Add failure detection (from *OVHcloud* or other parts) and avoid overwriting
  previous configuration files.
* Add tests.
* Add Dockerfile.
* Support multiple network interfaces.
