<?php

$exitWithMessage = function (string $message, int $exitCode = 1): void {
    fwrite(
        STDERR,
        $message . PHP_EOL
    );
    exit($exitCode);
};

require __DIR__ . '/../vendor/autoload.php';

$configurationDirPath = __DIR__ . '/../config';

if (is_readable($configurationDirPath . '/config.php')) {
    $CFG_BASE = require $configurationDirPath . '/config.php';
} else {
    $exitWithMessage(
        sprintf(
            'Failed to read script configuration file "%s".',
            $configurationDirPath . '/config.php'
        ),
        2
    );
}
if (is_readable($configurationDirPath . '/config.override.php')) {
    $CFG_OVERRIDE = require $configurationDirPath . '/config.override.php';
} else {
    $CFG_OVERRIDE = [];
}

$CFG = array_replace_recursive(
    $CFG_BASE,
    $CFG_OVERRIDE
);

if ($CFG === null) {
    $exitWithMessage('Failed to build script configuration.', 2);
}

$ovhReader = new \App\OvhIpRoutingReader($CFG);
$osConfigReader = new \App\OsNetworkConfigReader($CFG);
$netplanWriter = new \App\NetplanWriter($CFG);

$routedIps = $osConfig = $netplanConfigurations = null;

try {
    $routedIps = $ovhReader->getRoutedIps();
    $ipv6Gateway = $ovhReader->determineIpv6Gateway();
} catch (\Exception $e) {
    $exitWithMessage(sprintf(
        'An error occured when reading OVH IP routing: %s',
        $e->getMessage()
    ));
}

if (empty($routedIps)) {
    $exitWithMessage('Got no IP from OVH.');
} else {
    try {
        $osConfig = [
            'ipv4Gateway' => $osConfigReader->determineIpv4Gateway(),
            'dnsDomains' => $osConfigReader->determineResolvingDomainsSearchList(),
            'dnsServers' => $osConfigReader->determineResolvingNameserversAddresses(),
        ];
    } catch (\Exception $e) {
        $exitWithMessage(sprintf(
            'An error occured when reading OS network configuration: %s',
            $e->getMessage()
        ));
    }
}

if (count(array_filter($osConfig)) !== 3) {
    $exitWithMessage('Got no (or not enough) configuration from OS.');
} else {
    try {
        $netplanConfigurations = $netplanWriter->buildNetplanConfigurations(
            $routedIps['dedicated'],
            $routedIps['failover'] ?? [],
            $osConfig['ipv4Gateway'],
            $ipv6Gateway,
            $osConfig['dnsDomains'],
            $osConfig['dnsServers']
        );
    } catch (\Exception $e) {
        $exitWithMessage(sprintf(
            'An error occured when building Netplan configurations: %s',
            $e->getMessage()
        ));
    }
}

if (empty($netplanConfigurations)) {
    $exitWithMessage('Got no Netplan configuration.');
} else {
    try {
        $netplanWriter->writeNetplanConfigurations($netplanConfigurations);
    } catch (\Exception $e) {
        $exitWithMessage(sprintf(
            'An error occured when writing Netplan configurations: %s',
            $e->getMessage()
        ));
    }
}
$exitWithMessage('Script finished successfully.', 0);
