<?php

return [
    'ovh_api' => [
        'endpoint' => '***set_me***',
        'auth' => [
            'application_key' => '***set_me***',
            'application_secret' => '***secret***',
            'consumer_key' => '***set_me***',
        ],
    ],
    // 'host' => [
    //     'service_name' => '***set_me***',
    //     'service_name_autodetect' => false,
    // ],
    'netplan' => [
        'config_store' => [
            'directory' => __DIR__ . '/../netplan-files-example',
        ],
        'interface' => '***set_me***',
    ]
];
