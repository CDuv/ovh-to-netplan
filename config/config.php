<?php

return [
    'ovh_api' => [
        'endpoint' => '***set_me***',
        'auth' => [
            'application_key' => '***set_me***',
            'application_secret' => '***secret***',
            'consumer_key' => '***set_me***',
        ],
    ],
    'host' => [
        /**
         * The serviceName of current host (server executing this script).
         * Ignored if service_name_autodetect = bool(true).
         * @var string|null
         */
        'service_name' => null,
        /**
         * Tells if the host's serviceName should be auto-detected (true) or not (false).
         * @var bool
         */
        'service_name_autodetect' => true,
    ],
    'netplan' => [
        'config_store' => [
            /**
             * The path to the directory where to store the generated Netplan
             * configuration files. Directory will be created if missing.
             * @var string
             */
            'directory' => '/etc/netplan',

            /**
             * The pattern on how to name the Netplan configuration files.
             * Passed variables are (in this order):
             * * The order of priority of the configuration.
             * * The name of the configuration.
             * See printf() for documentation on format.
             * @var string
             */
            'filename_pattern' => '%1$d-%2$s.yaml',
        ],

        'dns_resolving' => [
            /**
             * The list of domains to use for host-name lookup (search list).
             * Ignored if search_domains_autodetect = bool(true).
             * @var array
             */
            'search_domains' => [],
            /**
             * Tells if the list of domains should be auto-detected (true) or
             * not (false) from operating system configuration.
             * @var bool
             */
            'search_domains_autodetect' => true,

            /**
             * The list of DNS servers to use for FQDN resolution.
             * Ignored if servers_autodetect = bool(true).
             * @var array
             */
            'servers' => [],
            /**
             * Tells if the list of domains should be auto-detected (true) or
             * not (false) from operating system configuration.
             * @var bool
             */
            'servers_autodetect' => true,
        ],

        /**
         * Name of the network device/interface.
         * Examples: eth0, eno1, enp2s0, …
         * @var string
         */
        'interface' => '***set_me***',

        'routing' => [
            /**
             * The gateway to use for IPv4.
             * Ignored if ipv4_gateway_autodetect = bool(true).
             * @var string|null
             */
            'ipv4_gateway' => null,
            /**
             * The gateway to use for IPv6.
             * Ignored if ipv6_gateway_autodetect = bool(true).
             * @var string|null
             */
            'ipv6_gateway' => null,
            /**
             * Tells if the gateway for IPv4 must be auto-detected (true) or
             * not (false) from operating system configuration.
             * @var bool
             */
            'ipv4_gateway_autodetect' => true,
            /**
             * Tells if the gateway for IPv4 must be auto-detected (true) or
             * not (false) from operating system configuration.
             * @var bool
             */
            'ipv6_gateway_autodetect' => true,
        ],
    ]
];
