<?php

namespace App;

use Ovh\Api as OvhApiClient;

/**
 * Class full of helper methods.
 */
class Helper
{
    /**
     * Get the endpoint of an OVH API client.
     *
     * @return string
     */
    public function getOvhApiEndpoint(OvhApiClient $client): string
    {
        $endpointProperty = new \ReflectionProperty(OvhApiClient::class, 'endpoint');
        $endpointProperty->setAccessible(true);

        return $endpointProperty->getValue($client);
    }

    /**
     * Get the "host" part of an URI.
     *
     * @return string|null
     */
    public function getUriHostPart(string $uri): ?string
    {
        $host = parse_url($uri, PHP_URL_HOST);
        if (!is_string($host)) {
            throw new \UnexpectedValueException(sprintf(
                'Failed to extract host part from URL "%s".',
                $uri
            ));
        } else {
            return $host;
        }
    }
}
