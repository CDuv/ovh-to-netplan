<?php

namespace App;

use App\Ovh\IpAddress as OvhIpAddress;
use IPLib\Range\Single as PhpIpAddressWithoutMask;
use IPLib\Range\Subnet as PhpIpAddressWithMask;
use IPLib\Factory as PhpIpAddressFactory;

/**
 * IP address model for the application.
 *
 * Internally, it's a wrapper around \IPLib\Range\Subnet.
 */
class IpAddress
{
    /**
     * @var PhpIpAddressWithMask
     */
    protected $ipObject;

    /**
     * @param PhpIpAddressWithMask|OvhIpAddress|string $ip
     */
    public function __construct($ip)
    {
        if ($ip instanceof PhpIpAddressWithMask) {
            $this->ipObject = $ip;
        } elseif ($ip instanceof OvhIpAddress) {
            $this->ipObject = $ip->getIpObject()->ipObject;
        } elseif (is_string($ip)) {
            $ipAddress = static::validateAndGetIpAddress($ip);
            if ($ipAddress === null) {
                throw new \InvalidArgumentException('Provided IP address parameter is a string but not a valid/recognized IP address.');
            }
            $this->ipObject = $ipAddress->ipObject;
        } else {
            throw new \InvalidArgumentException('The type of provided IP address parameter is not supported.');
        }
    }

    /**
     * Proxy to $this->ipObject
     */
    public function __call(string $name, array $arguments)
    {
        if (method_exists($this->ipObject, $name)) {
            return $this->ipObject->$name(...$arguments);
        } elseif (method_exists($this->ipObject->getStartAddress(), $name)) { // Try on first address
            return $this->ipObject->getStartAddress()->$name(...$arguments);
        } else {
            // Call the undefined method anyway, to get a proper PHP Fatal error…
            $this->ipObject->$name(...$arguments);
        }
    }

    /**
     * Get the IP address as string without any network mask.
     *
     * @return string
     */
    public function withoutMask(): string
    {
        return (string) $this->ipObject->getStartAddress();
    }

    public function __toString(): string
    {
        return (string) $this->ipObject;
    }

    /**
     * Tells if given IP address is the same as current instance.
     *
     * @return bool
     */
    public function equalsTo(self $ip): bool
    {
        if ($this->getAddressType() !== $ip->getAddressType()) {
            return false;
        }

        return $this->getBits() === $ip->getBits();
    }

    /**
     * Validate a string representing an IP address.
     *
     * @param string $ipAddress The IP address (as a string) to validate.
     *
     * @return string|null  If $ipAddress is a valid IP address, returns the
     *                      string representation of $ipAddress.
     */
    public static function validateAndGetIpAddressString(string $ipAddress): ?string
    {
        $ipAddressObject = static::validateAndGetIpAddress($ipAddress);
        if ($ipAddressObject === null) {
            return null;
        } else {
            return (string) $ipAddressObject;
        }
    }

    /**
     * Validate an IP address and return the IP as an instance of IpAddress.
     *
     * @param self|PhpIpAddressWithMask|OvhIpAddress|string $ipAddress  The IP address to validate.
     *
     * @return self|null If $ipAddress is a valid IP address, returns the IP
     *                   address object obtained from $ipAddress
     */
    public static function validateAndGetIpAddress($ipAddress): ?self
    {
        if ($ipAddress instanceof self) {
            // echo 'DEBUG: ' . __METHOD__ . '(): $ipAddress is self' . PHP_EOL;
            return $ipAddress;
        } elseif ($ipAddress instanceof OvhIpAddress) {
            // echo 'DEBUG: ' . __METHOD__ . '(): $ipAddress is OvhIpAddress' . PHP_EOL;
            return $ipAddress->getIpObject();
        } elseif (is_string($ipAddress)) {
            // echo 'DEBUG: ' . __METHOD__ . '(): $ipAddress is string (' . $ipAddress . ')' . PHP_EOL;
            // Returns either a PhpIpAddressWithoutMask or a PhpIpAddressWithMask
            $object = PhpIpAddressFactory::parseRangeString($ipAddress);

            if ($object === null) {
                return null;
            } elseif ($object instanceof PhpIpAddressWithoutMask) {
                // Convert to subnet to force mask
                return new self($object->asSubnet());
            } else {
                return new self($object);
            }
        } else {
            return null;
        }
    }
}
