<?php

namespace App;

/**
 * Trait for adding a convenient log() method to LoggerAwareInterface objects.
 *
 * Will use the logger if any, stdout otherwise.
 */
trait LoggerProxyTrait
{
    /**
     * Proxy to the logger, prints message to stdout if no logger.
     *
     * @param string $level
     * @param string $message
     * @param array $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log(string $level, string $message, array $context = []): void
    {
        if ($this->logger !== null) {
            $this->logger->log($level, $message, $context);
        } else {
            printf(
                '[%s] %s: %s' . PHP_EOL,
                gmdate('c'),
                mb_strtoupper($level),
                $message
            );
        }
    }
}
