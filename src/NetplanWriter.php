<?php

namespace App;

use Psr\Log\LogLevel;

/**
 * Class that builds and writes Netplan configuration files.
 *
 * Current implementation only supports:
 * * IPv4
 * * Single network interface
 */
class NetplanWriter implements \Psr\Log\LoggerAwareInterface
{
    use \App\LoggerProxyTrait;
    use \Psr\Log\LoggerAwareTrait;

    /**
     * @var array
     */
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Builds the Netplan configurations with provided values.
     *
     * @param array<IpAddress> $baseIps     An array of the IP addresses for the
     *                                      base Netplan configuration.
     * @param array<IpAddress> $extraIps    An array of the IP addresses for the
     *                                      extra Netplan configuration.
     * @param IpAddress $ipv4Gateway        The IPv4 to the gateway.
     * @param IpAddress|null $ipv6Gateway   The IPv6 to the gateway.
     * @param array<string> $nsDomains      The "search" domains.
     * @param array<IpAddress> $nsAddresses The nameservers IP addresses.
     */
    public function buildNetplanConfigurations(
        array $baseIps,
        array $extraIps,
        IpAddress $ipv4Gateway,
        ?IpAddress $ipv6Gateway,
        array $nsDomains,
        array $nsAddresses
    ): array {
        $networkIface = $this->config['netplan']['interface'];

        $baseNetplanCfg = $this->buildBaseConfiguration($networkIface);
        $extraNetplanCfg = $this->buildExtraConfiguration($networkIface);

        $thisInstance = $this;
        $baseIps = array_map(
            function ($ip) use ($thisInstance) {
                // Hack for OVH API-obtained IPv4: they have a "/32" netmask, we
                // force it to a /24.
                return $this->replaceNetmaskIfIpv4($ip, 24);
            },
            $baseIps
        );

        // Insert provided values into configurations:
        $baseNetplanCfg['network']['ethernets'][$networkIface]['addresses'] = array_map('strval', $baseIps);
        $baseNetplanCfg['network']['ethernets'][$networkIface]['gateway4'] = $ipv4Gateway->withoutMask();
        $baseNetplanCfg['network']['ethernets'][$networkIface]['nameservers'] = $this->buildNameserversConfig(
            $nsDomains,
            $nsAddresses
        );

        // Add IPv6 gateway informations:
        if ($ipv6Gateway !== null) {
            $baseNetplanCfg['network']['ethernets'][$networkIface]['gateway6'] = $ipv6Gateway->withoutMask();
            $baseNetplanCfg['network']['ethernets'][$networkIface]['routes'][] = [
                'to' => $ipv6Gateway->withoutMask(),
                'scope' => 'link',
            ];
        }

        $extraNetplanCfg['network']['ethernets'][$networkIface]['addresses'] = array_map(
            'strval',
            // [$this, 'replaceNetmaskIfIpv4'],
            $extraIps
        );

        return [
            [
                'name' => 'base',
                'content' => $baseNetplanCfg,
            ],
            [
                'name' => 'extra',
                'content' => $extraNetplanCfg,
            ],
        ];
    }

    /**
     * Build the base configuration part for a Netplan configuration.
     *
     * @param string $networkInterface  The network interface to configure.
     *
     * @return array The Netplan configuration part.
     */
    public function buildBaseConfiguration(string $networkInterface): array
    {
        return [
            'network' => [
                'version' => 2,
                'renderer' => 'networkd',
                'ethernets' => [
                    $networkInterface => [
                        'addresses' => [],
                        'dhcp4' => false,
                        'dhcp6' => false,
                        'gateway4' => null,
                        'gateway6' => null,
                        'nameservers' => [],
                        'routes' => [],
                    ],
                ],
            ],
        ];
    }

    /**
     * Build the extra configuration part for a Netplan configuration.
     *
     * @param string $networkInterface  The network interface to configure.
     *
     * @return array The Netplan configuration part.
     */
    public function buildExtraConfiguration(string $networkInterface): array
    {
        return [
            'network' => [
                'ethernets' => [
                    $networkInterface => [
                        'addresses' => [],
                    ],
                ],
            ],
        ];
    }

    /**
     * Write Netplan configurations to disk.
     *
     * @param array $configurations The configurations to write. Each array
     *                              element has a "name" and content "key".
     *
     * @return void
     *
     * @throws \RuntimeException if an error occurs during writing.
     */
    public function writeNetplanConfigurations(
        array $configurations
    ): void {
        $configDestinationDirpath = $this->config['netplan']['config_store']['directory'];
        $filenamePattern = $this->config['netplan']['config_store']['filename_pattern'];

        foreach ($configurations as $currentConfigurationIdx => $currentConfiguration) {
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Got a Netplan configuration ordered %d and named "%s".',
                    $currentConfigurationIdx,
                    $currentConfiguration['name']
                )
            );

            $currentFilename = sprintf(
                $filenamePattern,
                $currentConfigurationIdx,
                $currentConfiguration['name']
            );
            $destinationFilepath = $this->config['netplan']['config_store']['directory'] . '/' . $currentFilename;
            $destinationDirpath = dirname($destinationFilepath);

            if (!file_exists($destinationDirpath)) {
                $this->log(
                    LogLevel::INFO,
                    sprintf(
                        'Creating non-existing directory %s…',
                        $destinationDirpath
                    )
                );
                mkdir($destinationDirpath, 0755, true);
            }

            $contentAsYaml = \yaml_emit($currentConfiguration['content']);

            $this->log(
                LogLevel::INFO,
                sprintf(
                    'Will write YAML configuration to "%s"…',
                    $destinationFilepath
                )
            );
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Will write the following YAML document %s to "%s":' . PHP_EOL
                    . '-----YAML:-----' . PHP_EOL
                    . '%s' . PHP_EOL
                    . '-----/YAML-----' . PHP_EOL,
                    $currentConfiguration['name'],
                    $destinationFilepath,
                    $contentAsYaml
                )
            );

            if (file_put_contents($destinationFilepath, $contentAsYaml) !== false) {
                $this->log(
                    LogLevel::DEBUG,
                    sprintf(
                        'YAML document was successfully written to file %s',
                        $destinationFilepath
                    )
                );
            } else {
                $errorMessage = sprintf(
                    'An error occured when writting YAML document to file %s',
                    $destinationFilepath
                );
                $this->log(LogLevel::ERROR, $errorMessage);
                throw new \RuntimeException($errorMessage);
            }
        }
    }

    /**
     * Builds the "network:ethernets:IFACE:nameservers" Netplan configuration part.
     *
     * @param array<string> $nsDomains      The "search" domains.
     * @param array<IpAddress> $nsAddresses The nameservers IP addresses.
     *
     * @return array
     */
    public function buildNameserversConfig(
        array $nsDomains,
        array $nsAddresses
    ): array {
        return [
            'search' => $nsDomains,
            'addresses' => array_map(
                [$this, 'replaceNetmask'],
                $nsAddresses
            ),
        ];
    }

    /**
     * Replace (or remove) the netmask of an IP address.
     *
     * @param IpAddress $ip             The IP address to work on.
     * @param int|null $maskReplacement The number of bit for the new mask (null
     *                                  to remove the mask).
     *
     * @return string   The string representation of the IP address
     */
    protected function replaceNetmask(IpAddress $ip, ?int $maskReplacement = null): string
    {
        return
            $ip->withoutMask()
            . ($maskReplacement === null ? '' : '/' . $maskReplacement)
        ;
    }

    /**
     * Replace (or remove) the netmask of an IPv4 address.
     *
     * @param IpAddress $ip             The IP address to work on.
     * @param int|null $maskReplacement The number of bit for the new mask (null
     *                                  to remove the mask).
     *
     * @return string   The string representation of the IP address
     */
    protected function replaceNetmaskIfIpv4(IpAddress $ip, ?int $maskReplacement = null): string
    {
        if ($ip->getAddressType() === \IPLib\Address\Type::T_IPv4) {
            return $this->replaceNetmask($ip, $maskReplacement);
        } else {
            return (string) $ip;
        }
    }

    /**
     * Replace (or remove) the netmask of an IPv6 address.
     *
     * @param IpAddress $ip             The IP address to work on.
     * @param int|null $maskReplacement The number of bit for the new mask (null
     *                                  to remove the mask).
     *
     * @return string   The string representation of the IP address
     */
    protected function replaceNetmaskIfIpv6(IpAddress $ip, ?int $maskReplacement = null): string
    {
        if ($ip->getAddressType() === \IPLib\Address\Type::T_IPv6) {
            return $this->replaceNetmask($ip, $maskReplacement);
        } else {
            return (string) $ip;
        }
    }
}
