<?php

namespace App;

use Psr\Log\LogLevel;
use Symfony\Component\Process\Process;

/**
 * Class that reads the Operating System network configuration.
 */
class OsNetworkConfigReader implements \Psr\Log\LoggerAwareInterface
{
    use \App\LoggerProxyTrait;
    use \Psr\Log\LoggerAwareTrait;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var OvhApiClient
     */
    protected $apiClient;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Determine the IPv4 gateway to use.
     *
     * Current implementation reads the output of "ip -json route" command.
     *
     * Note: here is a complete shell-only command using `jq`:
     *   ip -json route \
     *     | jq \
     *         --raw-output \
     *         '[ .[] | select(.dst == "default") ][0] | .gateway'
     *
     * @return IpAddress|null
     */
    public function determineIpv4Gateway(): ?IpAddress
    {
        $routingConfig = $this->config['netplan']['routing'];

        if (
            array_key_exists('ipv4_gateway_autodetect', $routingConfig)
            && $routingConfig['ipv4_gateway_autodetect']
        ) {
            $this->log(
                LogLevel::INFO,
                'Will perform an auto-detection of IPv4 gateway…'
            );

            $shellCommand = [
                'ip',
                '-json',
                'route',
            ];
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Will use the following shell command to auto-detect the IPv4 gateway: %s',
                    implode(' ', $shellCommand)
                )
            );

            $ipRouteProcess = new Process($shellCommand);
            $ipRouteProcess->run();

            $resolvOutput = trim($ipRouteProcess->getOutput());

            if (!$ipRouteProcess->isSuccessful()) {
                $errorMessage = sprintf(
                    'Failed to auto-detect IPv4 gateway, got error: %s',
                    $ipRouteProcess->getErrorOutput()
                );
                $this->log(LogLevel::ERROR, $errorMessage);
                throw new \RuntimeException($errorMessage);
            } elseif (!empty($resolvOutput)) {
                $routes = json_decode($resolvOutput);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $errorMessage = sprintf(
                        'Failed to auto-detect IP gateway, got JSON error: %s',
                        json_last_error_msg()
                    );
                    $this->log(LogLevel::ERROR, $errorMessage);
                    throw new \UnexpectedValueException($errorMessage);
                }

                $gateway = null;
                foreach ($routes as $currentRoute) {
                    if ($currentRoute->dst === 'default') {
                        $gateway = $currentRoute->gateway;
                        break;
                    }
                }
                if ($gateway === null) {
                    $this->log(
                        LogLevel::WARNING,
                        'IPv4 gateway auto-detection was successful but returned no gateway.'
                    );
                    return null;
                } else {
                    $this->log(
                        LogLevel::DEBUG,
                        sprintf(
                            'Auto-detected the following IPv4 gateway: %s',
                            $gateway
                        )
                    );
                    return new IpAddress($gateway);
                }
            } else {
                $this->log(
                    LogLevel::WARNING,
                    'Shell command for IPv4 gateway auto-detection was successful '
                    . 'but returned no gateway.'
                );
            }
        } else {
            $this->log(
                LogLevel::INFO,
                'Will use IPv4 gateway from configuration…'
            );

            return new IpAddress($routingConfig['ipv4_gateway']);
        }
    }

    /**
     * Determine the list of domains to use for host-name lookup (search list).
     *
     * Current implementation reads the "/etc/resolv.conf" file for the "search"
     * configuration option.
     * Possible alternative: the `systemd-resolve --status` command.
     *
     * @return array<string>
     */
    public function determineResolvingDomainsSearchList(): array
    {
        $dnsResolvingConfig = $this->config['netplan']['dns_resolving'];

        if (
            array_key_exists('search_domains_autodetect', $dnsResolvingConfig)
            && $dnsResolvingConfig['search_domains_autodetect']
        ) {
            $this->log(
                LogLevel::INFO,
                'Will perform an auto-detection of search domains to use for host-name lookup…'
            );

            // The shell command that reads and parses /etc/resolv.conf:
            $shellCommand = <<<'EOT'
            grep '^search' "/etc/resolv.conf"\
            | awk '{ for (i=1; i<=NF; i++) $i = $(i+1); NF-=1; print }'
EOT;
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Will use the following shell command to auto-detect the search domains: %s',
                    str_replace('\\' . PHP_EOL, ' ', $shellCommand)
                )
            );

            $resolvParsingProcess = new Process($shellCommand);
            $resolvParsingProcess->run();

            $resolvOutput = trim($resolvParsingProcess->getOutput());
            $resolvOutput = explode(PHP_EOL, $resolvOutput)[0]; // Only keep the first line

            if (!$resolvParsingProcess->isSuccessful()) {
                $errorMessage = sprintf(
                    'Failed to auto-detect search domains, got error: %s',
                    $resolvParsingProcess->getErrorOutput()
                );
                $this->log(LogLevel::ERROR, $errorMessage);
                throw new \RuntimeException($errorMessage);
            } elseif (!empty($resolvOutput)) {
                $domains = array_map('trim', explode(' ', $resolvOutput));
                $this->log(
                    LogLevel::DEBUG,
                    sprintf(
                        'Auto-detected the following search domains: %s',
                        implode(', ', $domains)
                    )
                );
                return array_unique($domains);
            } else {
                $this->log(
                    LogLevel::WARNING,
                    'Search domain auto-detection was successful but returned no domains.'
                );
                return [];
            }
        } else {
            $this->log(
                LogLevel::INFO,
                'Will use search domains from configuration…'
            );

            return array_unique($dnsResolvingConfig);
        }
    }

    /**
     * Determine the list of DNS servers to use for FQDN resolution.
     *
     * Current implementation reads the "/etc/resolv.conf" file for the "nameserver"
     * configuration option.
     * Possible alternative: the `systemd-resolve --status` command.
     *
     * @return array<IpAddress>
     */
    public function determineResolvingNameserversAddresses(): array
    {
        $dnsResolvingConfig = $this->config['netplan']['dns_resolving'];

        $servers = [];

        if (
            array_key_exists('servers_autodetect', $dnsResolvingConfig)
            && $dnsResolvingConfig['servers_autodetect']
        ) {
            $this->log(
                LogLevel::INFO,
                'Will perform an auto-detection of DNS servers…'
            );

            // The shell command that reads and parses /etc/resolv.conf:
            $shellCommand = <<<'EOT'
            grep '^nameserver' "/etc/resolv.conf"\
            | awk '{ for (i=1; i<=NF; i++) $i = $(i+1); NF-=1; print }'
EOT;
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Will use the following shell command to auto-detect the DNS servers: %s',
                    str_replace('\\' . PHP_EOL, ' ', $shellCommand)
                )
            );

            $resolvParsingProcess = new Process($shellCommand);
            $resolvParsingProcess->run();

            $resolvOutput = trim($resolvParsingProcess->getOutput());

            if (!$resolvParsingProcess->isSuccessful()) {
                $errorMessage = sprintf(
                    'Failed to auto-detect DNS servers, got error: %s',
                    $resolvParsingProcess->getErrorOutput()
                );
                $this->log(LogLevel::ERROR, $errorMessage);
                throw new \RuntimeException($errorMessage);
            } elseif (!empty($resolvOutput)) {
                $servers = array_map('trim', explode(PHP_EOL, $resolvOutput));
                $this->log(
                    LogLevel::DEBUG,
                    sprintf(
                        'Auto-detected the following DNS servers: %s',
                        implode(', ', $servers)
                    )
                );
            } else {
                $this->log(
                    LogLevel::WARNING,
                    'Search domain auto-detection was successful but returned no domains.'
                );
            }
        } else {
            $this->log(
                LogLevel::INFO,
                'Will use DNS servers from configuration…'
            );

            $servers = $dnsResolvingConfig['servers'];
        }

        $servers = array_unique($servers);

        foreach (array_keys($servers) as $currentServerIdx) {
            $servers[$currentServerIdx] = new IpAddress($servers[$currentServerIdx]);
        }

        return $servers;
    }
}
