<?php

namespace App\Ovh;

class AbstractResource implements \ArrayAccess, \IteratorAggregate
{
    protected $dataContainer;

    public function __construct($data)
    {
        $this->dataContainer = $data;
    }

    public function getRawData(): array
    {
        return $this->dataContainer;
    }

    public function __set(string $name, $value): void
    {
        $this->offsetSet($name, $value);
    }

    public function __get(string $name)
    {
        return $this->offsetGet($name);
    }

    public function __isset(string $name): bool
    {
        return $this->offsetExists($name);
    }

    public function __unset(string $name): void
    {
        $this->offsetUnset($name);
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->dataContainer);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->dataContainer[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->dataContainer[$offset]) ? $this->dataContainer[$offset] : null;
    }

    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->dataContainer[] = $value;
        } else {
            $this->dataContainer[$offset] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->dataContainer[$offset]);
    }
}
