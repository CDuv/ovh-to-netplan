<?php

namespace App\Ovh;

use App\IpAddress as AppIpAddress;
use Ovh\Api as OvhApiClient;

class DedicatedServer extends \App\Ovh\AbstractResource
{
    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): string
    {
        return $this->serverId;
    }

    public function getMainIp(): string
    {
        return $this->ip;
    }

    public function getReverseName(): string
    {
        return $this->reverse;
    }

    /**
     * Get the IPs of the server
     *
     * @return array<AppIpAddress>
     */
    public function getIps(OvhApiClient $client): array
    {
        $ips = [];

        $ipsList = $client->get(sprintf(
            '/dedicated/server/%s/ips',
            $this->getName()
        ));
        foreach ($ipsList as $currentIp) {
            $ips[] = new AppIpAddress($currentIp);
        }

        return $ips;
    }
}
