<?php

namespace App\Ovh;

class IpAddress extends \App\Ovh\AbstractResource
{
    const TYPE_CDN = 'cdn';
    const TYPE_CLOUD = 'cloud';
    const TYPE_DEDICATED = 'dedicated';
    const TYPE_FAILOVER = 'failover';
    const TYPE_HOSTED_SSL = 'hosted_ssl';
    const TYPE_HOUSING = 'housing';
    const TYPE_LOADBALANCING = 'loadBalancing';
    const TYPE_MAIL = 'mail';
    const TYPE_OVERTHEBOX = 'overthebox';
    const TYPE_PCC = 'pcc';
    const TYPE_PCI = 'pci';
    const TYPE_PRIVATE = 'private';
    const TYPE_VPN = 'vpn';
    const TYPE_VPS = 'vps';
    const TYPE_VRACK = 'vrack';
    const TYPE_XDSL = 'xdsl';

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getIpObject(): \App\IpAddress
    {
        return new \App\IpAddress($this->ip);
    }

    public function getRoutedTo(): array
    {
        return $this->routedTo;
    }

    public function getRoutedToServiceName(): ?string
    {
        $routedTo = $this->getRoutedTo();
        if (empty($routedTo)) {
            throw new \OutOfBoundsException(
                'IP is not routed to anything, cannot get a serviceName'
            );
        } else {
            return $routedTo['serviceName'];
        }
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function __toString(): string
    {
        return (string) $this->getIpObject();
    }
}
