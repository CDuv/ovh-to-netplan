<?php

namespace App\Ovh;

use App\IpAddress as AppIpAddress;
use App\Ovh\IpAddress as OvhIpAddress;
use Ovh\Api as OvhApiClient;

class OvhApiClientHelper
{
    public static function create(array $config): OvhApiClient
    {
        return new OvhApiClient(
            $config['application_key'],
            $config['application_secret'],
            $config['api_endpoint'],
            $config['consumer_key']
        );
    }

    /**
     * Uses the "/dedicated/server/{serviceName}" endpoint.
     *
     * @param OvhApiClient $client      The API client to use.
     */
    public static function getDedicatedServer(OvhApiClient $client, string $serviceName): ?DedicatedServer
    {
        $foundItem = $client->get(sprintf(
            '/dedicated/server/%s',
            urlencode($serviceName)
        ));

        if ($foundItem === null) {
            return null;
        } else {
            return new DedicatedServer($foundItem);
        }
    }

    /**
     * Uses the "/dedicated/server" endpoint.
     *
     * @param OvhApiClient $client      The API client to use.
     *
     * @return array<DedicatedServer>
     */
    public static function getDedicatedServers(OvhApiClient $client): array
    {
        $resultSet = $client->get(
            '/dedicated/server'
        );

        $return = [];
        foreach ($resultSet as $currentItem) {
            $return[] = new DedicatedServer($currentItem);
        }
        return $return;
    }

    /**
     * Uses the "/ip/{ip}" endpoint.
     *
     * @param OvhApiClient $client      The API client to use.
     * @param AppIpAddress|OvhIpAddress|string $ip
     *
     * @return OvhIpAddress|null    The IP address.
     */
    public static function getIpAddress(OvhApiClient $client, $ip): ?OvhIpAddress
    {
        $ipString = (string) AppIpAddress::validateAndGetIpAddress($ip);

        $foundItem = $client->get(sprintf(
            '/ip/%s',
            urlencode($ipString)
        ));

        if ($foundItem === null) {
            return null;
        } else {
            return new OvhIpAddress($foundItem);
        }
    }

    /**
     * Uses the "/ip" endpoint to get IP addresses matching $filters.
     *
     * Example:
     *   OvhApiClientHelper::getIpAddresses(
     *       $someApiClient,
     *       [
     *           'routedTo.serviceName' => 'some_server_name',
     *           'type' => OvhIpAddress::TYPE_FAILOVER,
     *       ]
     *   );
     *
     * @param OvhApiClient $client      The API client to use.
     * @param array|null $filters       The optional filters to use when querying the API.
     *
     * @return array<OvhIpAddress>
     */
    public static function getIpAddresses(
        OvhApiClient $client,
        ?array $filters = null
    ): array {
        $resultSet = $client->get(
            '/ip',
            $filters
        );

        $return = [];
        foreach ($resultSet as $currentItem) {
            $return[] = self::getIpAddress($client, $currentItem);
        }
        return $return;
    }

    /**
     * Uses the "/ip" endpoint to get IP addresses matching $filters, as strings.
     *
     * @param OvhApiClient $client      The API client to use.
     * @param array|null $filters       The optional filters to use when querying the API.
     *
     * @return array<string>
     */
    public static function getIpAddressesStrings(
        OvhApiClient $client,
        ?array $filters = null
    ): array {
        $resultSet = $client->get(
            '/ip',
            $filters
        );

        $return = [];
        foreach ($resultSet as $currentItem) {
            $return[] = $currentItem;
        }
        return $return;
    }

    /**
     * Uses the "/ip" endpoint to get IP addresses matching $filters, grouped
     * using a key obtained from a callable.
     *
     * @param OvhApiClient $client      The API client to use.
     * @param callable $groupKeyGetter  The function that returns the key to group by (as a string).
     * @param array|null $filters       The optional filters to use when querying the API.
     *
     * @return array<OvhIpAddress>
     */
    public static function getIpAddressesGroupedBy(
        OvhApiClient $client,
        callable $groupKeyGetter,
        ?array $filters = null
    ): array {
        $resultSet = self::getIpAddresses($client, $filters);

        $return = [];
        foreach ($resultSet as $currentItem) {
            $itemGroup = $groupKeyGetter($currentItem);
            if (!is_string($itemGroup)) {
                throw new \TypeError(
                    sprintf(
                        'The groupKeyGetter must return a string, got a %s',
                        \gettype($itemGroup)
                    )
                );
            } else {
                if (!\array_key_exists($itemGroup, $return)) {
                    $return[$itemGroup] = [];
                }
                $return[$itemGroup][] = new OvhIpAddress($currentItem);
            }
        }
        return $return;
    }

    /**
     * Get the dedicated server where a given IP is routed to.
     *
     * @param AppIpAddress|OvhIpAddress|string $ip
     */
    public static function findDedicatedServerByIp(OvhApiClient $client, $ip): ?DedicatedServer
    {
        $foundServiceName = static::findDedicatedServerServiceNameByIp($client, $ip);

        if ($foundServiceName === null) {
            return null;
        } else {
            return static::getDedicatedServer($client, $foundServiceName);
        }
    }

    /**
     * Get the "serviceName" of the dedicated server where a given IP address
     * is routed to.
     *
     * @param AppIpAddress|OvhIpAddress|string $ip
     */
    public static function findDedicatedServerServiceNameByIp(OvhApiClient $client, $ip): ?string
    {
        $foundIp = static::getIpAddress($client, $ip);

        if ($foundIp === null) {
            return null;
        } else {
            return $foundIp->getRoutedToServiceName();
        }
    }
}
