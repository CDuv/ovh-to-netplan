<?php

namespace App;

use App\Helper as AppHelper;
use App\IpAddress as AppIpAddress;
use App\Ovh\DedicatedServer;
use App\Ovh\IpAddress as OvhIpAddress;
use App\Ovh\OvhApiClientHelper;
use Ovh\Api as OvhApiClient;
use Psr\Log\LogLevel;

/**
 * Class that reads the OVH routing configuration of current OVH dedicated server.
 */
class OvhIpRoutingReader implements \Psr\Log\LoggerAwareInterface
{
    use \App\LoggerProxyTrait;
    use \Psr\Log\LoggerAwareTrait;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var OvhApiClient
     */
    protected $apiClient;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Get all IP addresses routing to current OVH dedicated server.
     *
     * @return array<AppIpAddress>  An array of all the IP addresses of the runtime server.
     */
    public function getRoutedIps(): array
    {
        $runtimeOvhDedicatedServer = $this->determineRuntimeServer();
        $this->log(
            LogLevel::INFO,
            sprintf(
                'Determined current OVH server is: %s (%s).',
                $runtimeOvhDedicatedServer->getName(),
                $runtimeOvhDedicatedServer->getReverseName()
            )
        );

        $serverIps = [];
        $serverOvhIps = $this->getServerIpsByType($runtimeOvhDedicatedServer);

        foreach (array_keys($serverOvhIps) as $currentIpType) {
            $this->log(
                LogLevel::INFO,
                sprintf(
                    'Got %d IP addresses of type %s for current OVH server %s.',
                    count($serverOvhIps[$currentIpType]),
                    $currentIpType,
                    $runtimeOvhDedicatedServer->getName()
                )
            );
            if (!\array_key_exists($currentIpType, $serverIps)) {
                $serverIps[$currentIpType] = [];
            }
            foreach ($serverOvhIps[$currentIpType] as $currentIpAddress) {
                $serverIps[$currentIpType][] = new AppIpAddress($currentIpAddress);
            }
        }

        return $serverIps;
    }

    /**
     * Get an client for the OVH API (will re-use existing one if any).
     *
     * @return OvhApiClient
     */
    public function getApiClient(): OvhApiClient
    {
        if ($this->apiClient === null) {
            $this->apiClient = OvhApiClientHelper::create([
                'api_endpoint' => $this->config['ovh_api']['endpoint'],
                'application_key' => $this->config['ovh_api']['auth']['application_key'],
                'application_secret' => $this->config['ovh_api']['auth']['application_secret'],
                'consumer_key' => $this->config['ovh_api']['auth']['consumer_key'],
            ]);
        }
        return $this->apiClient;
    }

    /**
     * Determine the dedicated server this script is running on.
     *
     * @return DedicatedServer
     */
    protected function determineRuntimeServer(): DedicatedServer
    {
        $helper = new AppHelper();
        $runtimeServiceName = null;

        if ($this->config['host']['service_name_autodetect']) {
            $this->log(
                LogLevel::INFO,
                'Will perform an auto-detection of current runtime server…'
            );

            $endpoint = $helper->getOvhApiEndpoint($this->getApiClient());
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Will use OVH API endpoint "%s" as destination for '
                    . ' auto-detection of current runtime server…',
                    $endpoint
                )
            );

            $endpointHost = $helper->getUriHostPart($endpoint);
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Determined hostname "%s" (from OVH API endpoint "%s").',
                    $endpointHost,
                    $endpoint
                )
            );

            $hostIp = $this->getRuntimeIpAddress($endpointHost);
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Auto-detected source IP "%s" when using destination "%s".',
                    $hostIp,
                    $endpointHost
                )
            );

            $runtimeServiceName = OvhApiClientHelper::findDedicatedServerServiceNameByIp(
                $this->getApiClient(),
                $hostIp
            );
            if ($runtimeServiceName === null) {
                throw new \RuntimeException(
                    'Failed to get the service name from API using IP %s.',
                    $hostIp
                );
            } else {
                $this->log(
                    LogLevel::DEBUG,
                    sprintf(
                        'Found service name "%s" from IP %s.',
                        $runtimeServiceName,
                        $hostIp
                    )
                );
            }
        } elseif ($this->config['host']['service_name'] === null) {
            throw new \RuntimeException('No service name provided.');
        } else {
            $runtimeServiceName = $this->config['host']['service_name'];
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Using service name "%s" from configuration.',
                    $runtimeServiceName
                )
            );
        }

        $server = OvhApiClientHelper::getDedicatedServer(
            $this->getApiClient(),
            $runtimeServiceName
        );

        if ($server == null) {
            throw new \RuntimeException(sprintf(
                'Failed to get dedicated server of serviceName %s.',
                $runtimeServiceName
            ));
        } else {
            $this->log(
                LogLevel::DEBUG,
                sprintf(
                    'Got server "%s".',
                    $runtimeServiceName
                )
            );
            return $server;
        }
    }

    /**
     * Get the IP addresses routed to a server, grouped by types.
     *
     * @param DedicatedServer $server   The server for which we want the IP addresses.
     *
     * @return array<AppIpAddress>  An array of all the IP addresses of $server, grouped by types.
     */
    protected function getServerIpsByType(DedicatedServer $server): array
    {
        $failoverIps = [];
        $apiClient = $this->getApiClient();

        $this->log(
            LogLevel::DEBUG,
            sprintf(
                'Fetching informations about IP addresses of server %s from OVH API…',
                $server->getName()
            )
        );

        $ipGroups = OvhApiClientHelper::getIpAddressesGroupedBy(
            $apiClient,
            function (OvhIpAddress $item): string {
                return $item->getType();
            },
            [
                'routedTo.serviceName' => $server->getName(),
            ]
        );

        $this->log(
            LogLevel::DEBUG,
            sprintf(
                'Got %d groups of IP addresses of server %s from OVH API: %s',
                count($ipGroups),
                $server->getName(),
                implode(', ', array_keys($ipGroups))
            )
        );

        return $ipGroups;
    }

    /**
     * Determine output/source IP of current host.
     *
     * This methods opens an UDP socket on port 53 (DNS) towards an external
     * host only to grab the source IP from the network stack.
     *
     * @param string    $external_host
     *
     * @return string|null  The source IP address
     */
    protected function getRuntimeIpAddress(string $external_host): ?string
    {
        $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_connect($sock, $external_host, 53);
        socket_getsockname($sock, $name); // $name is passed by reference

        return $name;
    }

    /**
     * Determine the IPv6 gateway to use.
     *
     * Current implementation computes the IPv6 gateway from the IPv6 dedicated
     * address of the server following OVH's documentation:
     * https://docs.ovh.com/fr/dedicated/network-ipv6/
     *
     * It takes the IPv6 dedicated address and changes the 8th byte to "ff" and
     * the 5-8 groups to "00ff".
     * Example:
     * * IPv6: 2001:db8:1234:5678:9::abcd:ef
     * * Gwv6: 2001:db8:1234:56ff:ff:ff:ff:ff
     *
     * @return IpAddress|null
     */
    public function determineIpv6Gateway(): ?IpAddress
    {
        $routingConfig = $this->config['netplan']['routing'];

        if (
            array_key_exists('ipv6_gateway_autodetect', $routingConfig)
            && $routingConfig['ipv6_gateway_autodetect']
        ) {
            $this->log(
                LogLevel::INFO,
                'Will perform an auto-detection of IPv6 gateway…'
            );

            $runtimeOvhDedicatedServer = $this->determineRuntimeServer();
            $this->log(
                LogLevel::INFO,
                sprintf(
                    'Determined current OVH server is: %s (%s).',
                    $runtimeOvhDedicatedServer->getName(),
                    $runtimeOvhDedicatedServer->getReverseName()
                )
            );

            $apiClient = $this->getApiClient();
            $dedicatedIpAddresses = OvhApiClientHelper::getIpAddresses(
                $apiClient,
                [
                    'routedTo.serviceName' => $runtimeOvhDedicatedServer->getName(),
                    'type' => 'dedicated',
                ]
            );

            $ipv6DedicatedAddress = array_filter(
                $dedicatedIpAddresses,
                function (OvhIpAddress $item): string {
                    return $item->getIpObject()->getAddressType() === \IPLib\Address\Type::T_IPv6;
                }
            );
            if (count($ipv6DedicatedAddress) !== 1) { // Si erreur
                $errorMessage = sprintf(
                    'Failed to auto-detect IPv6 gateway, got no dedicated IPv6 '
                    . 'address for server: %s',
                    $runtimeOvhDedicatedServer->getName()
                );
                $this->log(LogLevel::ERROR, $errorMessage);
                throw new \RuntimeException($errorMessage);
            } else {
                $ipv6DedicatedAddress = reset($ipv6DedicatedAddress);
                $this->log(
                    LogLevel::DEBUG,
                    sprintf(
                        'Will use the following IPv6 address to compute the IPv6 gateway: %s',
                        $ipv6DedicatedAddress->getIpObject()->withoutMask()
                    )
                );

                // Apply OVH's transformation to get gateway from IP address
                $zerozeroInDec = hexdec('00');
                $ffInDec = hexdec('ff');
                $ovhGwBytesParts = [
                    7 => $ffInDec,
                    8 => $zerozeroInDec,
                    9 => $ffInDec,
                    10 => $zerozeroInDec,
                    11 => $ffInDec,
                    12 => $zerozeroInDec,
                    13 => $ffInDec,
                    14 => $zerozeroInDec,
                    15 => $ffInDec,
                ];
                $gatewayAddressBytes = $ovhGwBytesParts + $ipv6DedicatedAddress->getIpObject()->getBytes();

                // Build the IPv6 address from bytes
                $gateway = \IPLib\Address\IPv6::fromBytes($gatewayAddressBytes);

                if ($gateway === null) {
                    $this->log(
                        LogLevel::WARNING,
                        'IPv6 gateway auto-detection got dedicated IPv6 but'
                        . 'failed to compute the IPv6 gateway.'
                    );
                    return null;
                } else {
                    $this->log(
                        LogLevel::DEBUG,
                        sprintf(
                            'Auto-detected the following IPv6 gateway: %s',
                            $gateway
                        )
                    );
                    return new IpAddress((string) $gateway);
                }
            }
        } else {
            $this->log(
                LogLevel::INFO,
                'Will use IPv6 gateway from configuration…'
            );

            return new IpAddress($routingConfig['ipv6_gateway']);
        }
    }
}
